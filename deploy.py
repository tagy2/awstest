import subprocess
import os

def main():
    tag = os.getenv('TAG')
    server = {
        's': '"604876828602.dkr.ecr.eu-central-1.amazonaws.com/testrepo"',#"staging.server.adress.aws.amazon.com",
        'p': '"604876828602.dkr.ecr.eu-central-1.amazonaws.com/testrepo"'#"production.server.adress.aws.amazon.com"
    }.get(tag[-1], '"604876828602.dkr.ecr.eu-central-1.amazonaws.com/testrepo"')#"dev.server.adress.aws.amazon.com"
    print(server.strip('"'))

if __name__ == '__main__':
    main()
